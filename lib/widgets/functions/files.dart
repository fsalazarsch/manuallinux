import 'package:flutter/material.dart';
import 'dart:io';
import 'dart:typed_data';
import 'package:flutter/foundation.dart';
import 'package:path_provider/path_provider.dart';
import 'package:http/http.dart' as http;
import 'package:dio/dio.dart';
  
  Dio dio = new Dio();

  Future<String> get _localPath async {

    if (kIsWeb) {
      return "/assets/data/";
      
    } else {
      final directory = await getApplicationDocumentsDirectory();
      return directory.path;
    }
  }


  Future<String> get_root_path() async {
        final path = await _localPath;
        return path;
            }

  Future<String> downloadFile(String url, String fileName, bool flag) async {
        final path = await _localPath;
        final checkPathExistence = await Directory(path+'/'+fileName).exists();
          

        File file;
        String filePath = '';
        String myUrl = '';

        try {
          if (checkPathExistence == false)
            new File(path+'/'+fileName).create(recursive: true);

          if ((File('$path/$fileName').existsSync()) && (flag == false)){
          	debugPrint("El archivo $path/$fileName ya existe");
          }
          else{
          	myUrl = url+'/'+fileName;
            

          final resp = await http.get(Uri.parse(myUrl));
          	if(resp.statusCode == 200) {
    	        filePath = '$path/$fileName';
        	    file = File(filePath);
            	await file.writeAsString(resp.body);

				}
				else
		            filePath = 'Error code: '+resp.statusCode.toString()+ "\n Path : "+myUrl;
          }
        }
        catch(ex){
          debugPrint(ex.toString());
          filePath = 'Can not fetch url '+ myUrl;
        }
    	debugPrint(filePath);
        return filePath;
      
    }
