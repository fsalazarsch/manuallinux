import 'dart:core';
import 'package:flutter/material.dart';
import 'package:manuallinux/widgets/functions/files.dart';
import 'package:path_provider/path_provider.dart';
import 'dart:io';
import 'package:manuallinux/widgets/detail/DistroDetail.dart';


class ListViewDistros extends StatelessWidget {

  final List items; 
  final String nro;
  final String path;
  String localpath ="";

  ListViewDistros({
    required this.items, 
    required this.nro,
    required this.path
  });
    

  String name_img(item){
    if ( item["alias"] == null)
      return item["Nombre"].toLowerCase();
    else
      return item["alias"].toLowerCase();
  }


  Future<File> _loadImage( name ) async {
    final directory = await getApplicationDocumentsDirectory();
    final path = directory.path;
    final image = File('$path/img/$name.png');
    debugPrint('$path/img/$name.png');
    return image;
  }


  @override
  Widget build(BuildContext context) { 
    return ListView.builder(
      itemCount: items.length < int.parse(this.nro) ? items.length : int.parse(this.nro),
      padding: const EdgeInsets.all(8),
      itemBuilder: (context, index) {
        final item = items[index];
        return GestureDetector(
          onTap :() {
            Navigator.push(
              context,
              MaterialPageRoute(builder: (context) =>  DistroDetail(item: item, base: this.path)),
              );
            },
            child: Card(
              child: Column(
                children: <Widget>[                 
                ListTile(
                  leading:  
                  Image.network('https://raw.githubusercontent.com/fsalazarsch/linux_commands/master/img/'+name_img(item)+'.png',
                    height: 40, width: 40,
                    errorBuilder: (context, Object exception, StackTrace? stackTrace) {
                      return Image.asset("assets/img/tux.png");
                      },
                    ),
                  title: Text(item["Nombre"]),
                  subtitle: Text(item["Comienzo"]),
                  )],
                ),
              )
            );
        },
        );
  }
}