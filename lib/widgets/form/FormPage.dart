import 'package:flutter/material.dart';
import 'package:webview_flutter/webview_flutter.dart';
import 'package:webview_flutter_android/webview_flutter_android.dart';
import 'package:flutter_webview_plugin/flutter_webview_plugin.dart';

class FormPage extends StatelessWidget {
    const FormPage({Key? key}) : super(key: key);

  @override


  Widget build(BuildContext context) {
    return SafeArea(
      child: Padding(
        padding: EdgeInsets.only(top: 120),
        child: WebviewScaffold(
          url: 'http://docs.google.com/forms/d/e/1FAIpQLSfpqPrJysMduJPdB3yBu7lca00tJtgsc5tG-CyERlr-SWlwmg/viewform',
          appBar: AppBar(
            title: Text('Google Form'),
          ),
        ),
      ),
    );
  }
}




