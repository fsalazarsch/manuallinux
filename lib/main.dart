import 'package:flutter/material.dart';
import 'package:manuallinux/widgets/functions/files.dart';
import 'package:manuallinux/widgets/functions/CounterStorage.dart';
import 'dart:convert';
import 'package:flutter/services.dart';
import 'dart:io';
import 'package:path_provider/path_provider.dart';
import 'package:manuallinux/widgets/form/ManualForm.dart';
import 'package:manuallinux/widgets/form/DistroForm.dart';
import 'package:manuallinux/widgets/form/FormPage.dart';
import 'package:flutter/foundation.dart' show kIsWeb;
import 'package:webview_flutter/webview_flutter.dart';

void main() {

    WidgetsFlutterBinding.ensureInitialized();
  SystemChrome.setPreferredOrientations([DeviceOrientation.portraitUp])
    .then((_) {
      runApp(TabBarDemo());
    });
}

class TabBarDemo extends StatelessWidget {
  TabBarDemo({Key? key}) : super(key: key);

  final String root = "";

  void get_help( BuildContext context ){
    debugPrint("asdasd");
    Navigator.push(context, 
        MaterialPageRoute(builder: (context) => FormPage()),
      );
    
  }

  void get_data(BuildContext context, bool flag){


    const snackBar = SnackBar(
      content: Text('Datos importados'),
      );
    downloadFile("https://raw.githubusercontent.com/fsalazarsch/linux_commands/master", "data/distros.json", flag);    
    List<String> list = ['1', '1x', '2', '3', '4', '5', '6', '7', '8'];
    list.forEach((element) => downloadFile("https://raw.githubusercontent.com/fsalazarsch/linux_commands/master", "data/man"+element+".json", flag));
    if (flag == true)
      ScaffoldMessenger.of(context).showSnackBar(snackBar);
    }


  @override
  Widget build(BuildContext context) {
    get_data(context, false);
    return MaterialApp(
      home: DefaultTabController(
        length: 3,
        child: Scaffold(
          appBar: AppBar(
            actions: <Widget>[
            PopupMenuButton<String>(


              itemBuilder: (context) => [
              PopupMenuItem(
                child: Text("Actualizar datos"),
                onTap: () => get_data(context, true),
                ),
              ],
              ),
            ],
            bottom: const TabBar(
              tabs: [
                Tab(
                  text: "Manual",
                  icon: Icon(Icons.book)),
                Tab(
                  text: "Distros",
                  icon: ImageIcon(
                    AssetImage("assets/img/tux.png"),
                    color: Colors.white,
                    size: 30,
                    ),
                  ),
                Tab(
                  text: "Colaborar",
                  icon: Icon(Icons.back_hand)),
              ],
            ),
            title: const Text('Manual linux'),
          ),
          body: const TabBarView(
            children: [
              ManualForm(),
              DistroForm(),
              FormPage()
            ],

          ),

        ),
      ),
    );
  }

}