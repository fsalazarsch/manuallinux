/**
 * Automatically generated file. DO NOT MODIFY
 */
package com.nullpointerex.manuallinux;

public final class BuildConfig {
  public static final boolean DEBUG = false;
  public static final String APPLICATION_ID = "com.nullpointerex.manuallinux";
  public static final String BUILD_TYPE = "release";
  public static final int VERSION_CODE = 21;
  public static final String VERSION_NAME = "2.1.0";
}
